#!/bin/bash

clang-format --style=file -i ./src/**/*.{c,h}pp ./*.hpp
clang-tidy --fix ./src/**/*.cpp